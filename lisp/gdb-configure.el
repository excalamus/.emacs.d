(setq gdb-many-windows t)

(defun xc/gdb-setup-windows ()
  "Override the default window layout pattern for option `gdb-many-windows'."

  ;; create all possible buffers given by `gdb-buffer-rules'
  (gdb-get-buffer-create 'gdb-registers-buffer)
  (gdb-get-buffer-create 'gdb-locals-buffer)
  (gdb-get-buffer-create 'gdb-stack-buffer)
  (gdb-get-buffer-create 'gdb-disassembly-buffer)
  (gdb-get-buffer-create 'gdb-memory-buffer)
  (gdb-get-buffer-create 'gdb-threads-buffer)
  (gdb-get-buffer-create 'gdb-breakpoints-buffer)
  (gdb-get-buffer-create 'gdb-partial-output-buffer)

  ;; Define a window setup.
  ;;
  ;; +------------------+------------------+
  ;; |                  |                  |
  ;; |                  |     win1         |
  ;; |                  |                  |
  ;; |                  |                  |
  ;; |                  +------------------+
  ;; |                  |                  |
  ;; |                  |     win3         |
  ;; |     win0         |                  |
  ;; |                  |                  |
  ;; |                  +------------------+
  ;; |                  |                  |
  ;; |                  |     win2         |
  ;; |                  |                  |
  ;; |                  |                  |
  ;; +------------------+------------------+

  (delete-other-windows)
  (let* ((full-height (window-height))
         (third-height (/ full-height 3))
         win0
         win1
         win2
         win3
         )
    (setq win0 (selected-window))
    (setq win1 (split-window-right))
    (select-window win1)
    (setq win2 (split-window nil (- full-height third-height)))
    (setq win3 (split-window nil third-height))

    ;; gdb names several buffers after the program being debugged and
    ;; provides functions to get the buffer name.  How this happens
    ;; isn't the same for all buffers.  For example, the
    ;; gdb-memory-buffer created above is first called "limbo" before
    ;; the (gdb-memory-buffer-name) function returns anything.  You
    ;; would have to rename the memory buffer yourself.
    ;;
    ;; (gdb-registers-buffer-name)
    ;; (gdb-locals-buffer-name)
    ;; (gdb-stack-buffer-name)
    ;; (gdb-disassembly-buffer-name)
    ;; (gdb-threads-buffer-name)
    ;; (gdb-breakpoints-buffer-name)
    ;; (gdb-partial-output-name)
    ;; (gdb-memory-buffer-name)
    ;; gud-comint-buffer
    ;; gdb-main-file

    ;; win0
    (select-window win0)
    (switch-to-buffer
     (if gdb-main-file
         (gud-find-file gdb-main-file)
       ;; Put buffer list in window if we
       ;; can't find a source file.
       (list-buffers-noselect)))

    ;; win1
    (set-window-buffer win1 (gdb-locals-buffer-name))

    ;; win2
    (select-window win2)
    (switch-to-buffer gud-comint-buffer)

    ;; win3
    (select-window win3)
    (set-window-buffer win3 (gdb-stack-buffer-name))

    (select-window win2)
    ))

(defalias 'gdb-setup-windows #'xc/gdb-setup-windows)

;; (defun gdb-display-buffer (buf)
;; (switch-to-buffer buf))
