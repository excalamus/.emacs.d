;; prevent package.el loading packages prior to init-file loading
;; https://github.com/raxod502/straight.el#getting-started
(setq package-enable-at-startup nil)

;; Don't use straight.el for Org.  It struggles with Org mode and
;; errors constantly occur that require you to know how straight.el
;; internals work.  Instead, delay all package loading until Org is
;; loaded.  Force using the local Org repo.
;;
;; Call "make autoloads" per https://orgmode.org/manual/Installation.html
(let* ((buff (get-buffer-create "*xc/early-init*"))
       (exit-code
        (call-process "bash" nil buff nil "-c" "(cd /home/ahab/Projects/org-mode && make autoloads)")))
  (if (/= exit-code 0)
      (with-current-buffer buff
        (goto-char (point-min))
        (insert "Error loading Org from source in early-init.el.\n\n")
        (switch-to-buffer buff))
    (kill-buffer buff)))

(add-to-list 'load-path "/home/ahab/Projects/org-mode/lisp")
